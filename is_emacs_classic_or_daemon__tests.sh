#!/usr/bin/env bash

### Copyright (C) 2017 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### https://bitbucket.org/tlroche/emacs_helpers/src/HEAD/is_emacs_classic_or_daemon__tests.sh?at=master

### This script
### * attempts to test ./is_emacs_classic_or_daemon.sh
### * uses some probably-bash-specific code
### * should be runnable either
###   * as a "normal" called script
###   * `source`d, then individual functions called from commandline (but see TODO item below)
### * depends on
###   * functions in emacs_helper_functions.sh (which is `source`d below)
###   * `EMACS_CONFIG_DIR` set in emacs_helper_functions.sh
###   so be sure to correctly set `EMACS_CONFIG_DIR` and `HELPER_FP`

### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### TODO:
### * test on more than one {config, device}!
### * read {important, non-messaging} variables from properties file(s)
###   (so user need not edit this)
### * add string var=USAGE, support `--help`
### * support `--debug`
### * support `--version`

### ------------------------------------------------------------------
### constants
### ------------------------------------------------------------------

### Messaging constants

# echo "DEBUG: BASH_SOURCE='${BASH_SOURCE}'"

declare -r THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare -r THIS_FN="$(basename ${THIS_FP})"
declare -r THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"  # FQ path to caller's $(pwd)
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r VERSION_N='0.1'
declare -r VERSION_STR="${THIS_FN} version=${VERSION_N}"

### What we're testing

declare -r TARGET_NAME="${THIS_FN%__tests.sh}"
declare -r TARGET_FN="${TARGET_NAME}.sh"
declare -r TARGET_FP="${THIS_DIR}/${TARGET_FN}"

echo "${MESSAGE_PREFIX} DEBUG: test target='${TARGET_FP}'"

### What's helping us test

declare -r HELPER_FN='emacs_helper_functions.sh'
declare -r HELPER_FP="${THIS_DIR}/${HELPER_FN}"

### Emacs-related: adjust as needed for your {config, device}

# Note: must set EMACS_CONFIG_DIR in ${HELPER_FP}
# seconds to wait for process to startup fully
declare -r CLASSIC_WAIT_SECS=10
declare -r DAEMON_WAIT_SECS=10

### ------------------------------------------------------------------
### functions
### ------------------------------------------------------------------

source "${HELPER_FP}" # since functions from it are reused below

### Kill all processes with name ~= emacs* and remove all files.
function cleanup() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

    echo "${FN_MESSAGE_PREFIX} start cleanup @ $(date)"
    cleanup_emacs_processes
    cleanup_emacs_desktop_locks
    echo "${FN_MESSAGE_PREFIX}   end cleanup @ $(date)"
} # end function cleanup

### Initialize before running tests.
function setup() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    echo "${FN_MESSAGE_PREFIX} start setup @ $(date)" # debug
    ## check for executable target
    if   [[ -z "${TARGET_FP}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} failed to define test target TARGET_FP, exiting ..."
        if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
            exit 5
        else
            return 5
        fi
    elif [[ ! -x "${TARGET_FP}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} cannot execute test target '${TARGET_FP}', exiting ..."
        if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
            exit 6
        else
            return 6
        fi
    else
        ## remove old objects, prepare to run tests
        cleanup
    fi
#    echo "${FN_MESSAGE_PREFIX}   end setup @ $(date)" # debug
} # end function setup

### 'Classic' emacs is neither client nor daemon.
### TODO: refactor code copied with function=create_daemon
function create_classic() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

    ## following vars reused
    local pgrep_spew=''
    local emacs_process_n=0

#    echo "${FN_MESSAGE_PREFIX} start testing classic @ $(date)" # debug

    echo "${FN_MESSAGE_PREFIX} will attempt to start classic Emacs,"
    echo -e "\tthen wait ${CLASSIC_WAIT_SECS} s for it to start fully."

    command emacs --debug-init & # avoid user `alias emacs=whatever"
    sleep ${CLASSIC_WAIT_SECS}
    # check for processes
    local pgrep_spew="$(pgrep -l emacs)"  # text about processes, 1 line per process
    if [[ -z "${pgrep_spew}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} failed to start classic Emacs process, exiting ..."
        if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
            exit 7
        else
            return 7
        fi
    else
        emacs_process_n="$(echo "${pgrep_spew}" | wc -l)"
        if (( emacs_process_n > 1 )) ; then
            >&2 echo "${FN_ERROR_PREFIX} started |classic Emacs processes| > 1:"
            >&2 echo "${pgrep_spew}"
            >&2 echo "${FN_ERROR_PREFIX} exiting ..."
            if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
                exit 8
            else
                return 8
            fi
        else
            echo "${FN_MESSAGE_PREFIX} started classic Emacs process:"
            echo "${pgrep_spew}"
        fi
    fi

#    echo "${FN_MESSAGE_PREFIX}   end testing classic @ $(date)" # debug
} # end function create_classic

### ASSERT: no Emacs daemon is currently running.
### TODO: refactor code copied with function=test_daemon
function test_classic() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    local -r EXP_VALUE='"classic"'
    local -r EXP_VALUE='classic'
#    echo "${FN_MESSAGE_PREFIX} DEBUG: about to run test target='${TARGET_FP}'"
    local -r RET_VALUE="$(${TARGET_FP})"
#    echo "${FN_MESSAGE_PREFIX} DEBUG: test target returns '${RET_VALUE}'"

    echo # newline
    if   [[ -z "${RET_VALUE}" ]] ; then
        >2& echo "${FN_ERROR_PREFIX} FAILURE!"
        >2& echo "${FN_ERROR_PREFIX} received null or blank return value"
        >2& echo "${FN_ERROR_PREFIX} expected value='${EXP_VALUE}'"
        >2& echo # newline
    elif [[ "${EXP_VALUE}" == "${RET_VALUE}" ]] ; then
        echo "${FN_MESSAGE_PREFIX} success"
    else
        >2& echo "${FN_ERROR_PREFIX} FAILURE!"
        >2& echo "${FN_ERROR_PREFIX} received return value='${RET_VALUE}'"
        >2& echo "${FN_ERROR_PREFIX} expected value='${EXP_VALUE}'"
        >2& echo # newline
    fi
    echo # newline
} # end function test_classic

function create_and_test_classic() {
    create_classic
    test_classic
} # end function create_and_test_classic

### Start only daemon, not client + daemon
### TODO: refactor code copied with function=create_classic
function create_daemon() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

    ## following vars reused
    local pgrep_spew=''
    local emacs_process_n=0

#    echo "${FN_MESSAGE_PREFIX} start testing daemon @ $(date)" # debug

    echo "${FN_MESSAGE_PREFIX} will attempt to start Emacs daemon,"
    echo -e "\tthen wait ${DAEMON_WAIT_SECS} s for it to start fully."

    command emacs --daemon & # avoid user `alias emacs=whatever"
    sleep ${DAEMON_WAIT_SECS}
    # check for processes
    local pgrep_spew="$(pgrep -l emacs)"  # text about processes, 1 line per process
    if [[ -z "${pgrep_spew}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} failed to start Emacs daemon process, exiting ..."
        if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
            exit 7
        else
            return 7
        fi
    else
        emacs_process_n="$(echo "${pgrep_spew}" | wc -l)"
        if (( emacs_process_n > 1 )) ; then
            >&2 echo "${FN_ERROR_PREFIX} started |Emacs daemon processes| > 1:"
            >&2 echo "${pgrep_spew}"
            >&2 echo "${FN_ERROR_PREFIX} exiting ..."
            if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
                exit 8
            else
                return 8
            fi
        else
            echo "${FN_MESSAGE_PREFIX} started Emacs daemon process:"
            echo "${pgrep_spew}"
        fi
    fi

#    echo "${FN_MESSAGE_PREFIX}   end testing daemon @ $(date)" # debug
} # end function create_daemon

### ASSERT: no classic Emacs is currently running.
### TODO: refactor code copied with function=test_classic
function test_daemon() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local FN_ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    local -r EXP_VALUE='"daemon"'
    local -r EXP_VALUE='daemon'
    local -r RET_VALUE="$(${TARGET_FP})"
    echo "${FN_MESSAGE_PREFIX} DEBUG: test target returns '${RET_VALUE}'"

    echo # newline
    if   [[ -z "${RET_VALUE}" ]] ; then
        >2& echo "${FN_ERROR_PREFIX} FAILURE!"
        >2& echo "${FN_ERROR_PREFIX} received null or blank return value"
        >2& echo "${FN_ERROR_PREFIX} expected value='${EXP_VALUE}'"
        >2& echo # newline
    elif [[ "${EXP_VALUE}" == "${RET_VALUE}" ]] ; then
        echo "${FN_MESSAGE_PREFIX} success"
    else
        >2& echo "${FN_ERROR_PREFIX} FAILURE!"
        >2& echo "${FN_ERROR_PREFIX} received return value='${RET_VALUE}'"
        >2& echo "${FN_ERROR_PREFIX} expected value='${EXP_VALUE}'"
        >2& echo # newline
    fi
    echo # newline
} # end function test_daemon

function create_and_test_daemon() {
    create_daemon
    test_daemon
} # end function create_and_test_daemon

### Run all testcases.
function main() {
    setup
    create_and_test_classic
    cleanup
    create_and_test_daemon
    cleanup
} # end function main

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

main
exit 0
