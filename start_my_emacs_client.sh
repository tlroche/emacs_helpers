#!/usr/bin/env bash

### start Emacs client. TODO: harmonize with ./startMyEmacsDaemon.sh

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Parse commandline
THIS_FP="${0}"
THIS_FN="$(basename ${THIS_FP})"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"  # FQ path to caller's $(pwd)
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

### Functionality

EMACS_CONFIG_DIR="${HOME}/.emacs.d"

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

cd "${EMACS_CONFIG_DIR}"    # hmm ... why this?

## start backgrounded to avoid hogging terminal where I run this

# emacs --debug-init &           # used this going back as far as I can remember (Cygwin days)
# emacs-snapshot --debug-init &  # version=24 prior to the demise of emacs-snapshot
# emacs --debug-init &           # prior to switching to daemon
# emacsclient -c -a emacs &      # seems to cause problem if restarting with no daemon

# following per http://stackoverflow.com/a/21555855/915044 . note :
# `--alternate-editor ''` == iff Emacs server !running, then [`emacs --daemon`, connect]
# `--create-frame` == run GUI (vs `--tty` == run in terminal)

# emacsclient --alternate-editor '' --create-frame &              # works, but doesn't catch startup errors

exit 0
