#!/usr/bin/env bash

### Following used in answer @ https://emacs.stackexchange.com/a/36126 to question @ https://emacs.stackexchange.com/q/36044/5444
### Presuming one has process(es) with name=`emacs`, how to discriminate between
### 'classic' and 'daemon' processes (as defined in SE question above)?
### Following attempts to do this. Usage as described in section='testcases' below.
### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### TODO:
### * parameterize TEST_OUT as (failure) discussed below
### * add `--debug`

### Copyright (C) 2017 by Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### https://bitbucket.org/tlroche/emacs_helpers/src/HEAD/is_emacs_classic_or_daemon.sh?at=master&fileviewer=file-view-default

### ------------------------------------------------------------------
### constants
### ------------------------------------------------------------------

## Messaging constants

declare -r THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare -r THIS_FN="$(basename ${THIS_FP})"
declare -r THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))"  # FQ path to caller's $(pwd)
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r VERSION_N='0.1'
declare -r VERSION_STR="${THIS_FN} version=${VERSION_N}"

## output from elisp sent to ... whatever emacs daemon is listening.

## note need for backslash in following
declare -r TEST_OUT="$(emacsclient --alternate-editor='true' --\eval '(if (daemonp) (message "true") (message "false"))' 2> /dev/null)" # constant
# echo "debug: TEST_OUT='${TEST_OUT}'"

## all my attempts to parameterize the above fail--many more than the following :-(
# #ELISP='(if (daemonp) (message "true") (message "false"))'
# ELISP='(if (daemonp) (message \"true\") (message \"false\"))'
# echo "debug: ELISP='${ELISP}'"
# #TEST_OUT="$(emacsclient --alternate-editor='true' --eval '${ELISP}')"
# TEST_OUT="$(emacsclient --alternate-editor='true' --eval \'${ELISP}\')"
# echo "debug: TEST_OUT='${TEST_OUT}'"

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

## -----------------------------------------------------------------------
## payload
## -----------------------------------------------------------------------

if   [[ -z "${TEST_OUT}" ]] ; then
    echo 'classic'
    exit 0
elif [[ "${TEST_OUT}" == '"true"' ]] ; then
    echo 'daemon'
    exit 0
else
    >2& echo "${ERROR_PREFIX}: listening Emacs of unknown type (not classic or daemon)"
    exit 2 # save retval==1 for internal failure
fi

### ------------------------------------------------------------------
### execution fence
### ------------------------------------------------------------------

## I.e., don't run any following lines (to EOF), period.

if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
    # ... if this file is executed
    exit 3 # should never be reached
else
    # ... if this file is `source`d
    return 0
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

## -------------------------------------------------------------------
## executable
## -------------------------------------------------------------------

## goto ./is_emacs_classic_or_daemon__tests.sh

## -------------------------------------------------------------------
## examples
## -------------------------------------------------------------------

### Cut'n'paste any following stanza(s) into console after setting following constant:
THIS_FP='' # set to FQ path to this executable file

### Note sample/expected output from each line follows in comment (to each executable line).

### case=daemon

date ; emacs --daemon &
# > Thu Oct 12 22:08:01 MST 2017
# > [1] 5935
# > ... console spew follows ...

date ; pgrep -l emacs
# > Thu Oct 12 22:08:25 MST 2017
# > 5935 emacs

date ; "${THIS_FP}"
# > Thu Oct 12 22:08:29 MST 2017
# > daemon

### cleanup

date ; pkill -9 emacs
# > Thu Oct 12 22:08:35 MST 2017

date ; find ~/.emacs.d/ -name '*lock*'
# > Thu Oct 12 22:08:40 MST 2017
# > /home/me/.emacs.d/personal/.emacs.desktop.lock

date ; find ~/.emacs.d/ -name '*lock*' | xargs rm
# > Thu Oct 12 22:08:45 MST 2017

date ; pgrep -l emacs
# > Thu Oct 12 22:08:49 MST 2017

### case=classic

date ; emacs --debug-init &
# > Thu Oct 12 22:09:05 MST 2017
# > [1] 18224

date ; pgrep -l emacs
# > Thu Oct 12 22:09:44 MST 2017
# > 18224 emacs

date ; "${THIS_FP}"
# > Thu Oct 12 22:09:48 MST 2017
# > classic
